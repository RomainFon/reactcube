import React, { Component } from 'react';
import './SelectBox.scss';

class SelectBox extends Component {
    constructor(props){
        super(props);
    }

    createOption(data){
        return(
            <option key={data.value} value={data.value}>{data.label}</option>
        );
    }

    onChange(e){
        this.setState({
            value: e.currentTarget.value
        });
        if(this.props.onChange){
            this.props.onChange(Number(this.state.value));
        }
    }

    render() {
        let options = [];

        options.push(this.createOption({
            label: 'RED',
            value: '0xff0000'
        }));
        options.push(this.createOption({
            label: 'BLUE',
            value: '0x0000ff'
        }));
        return (
            <select className="SelectBox" onChange ={(e) => this.props.onChange(e.currentTarget.value)}>{options}</select>
        );
    }
}

export default SelectBox;
