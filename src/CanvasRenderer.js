import React, { Component } from 'react';
import './CanvasRenderer.scss';
import * as THREE from "three";

class CanvasRenderer extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        //SCENE
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

        this.renderer = new THREE.WebGLRenderer({
            canvas: this.refs.canvas
        });
        this.renderer.setSize( window.innerWidth, window.innerHeight );

        //CUBE
        let geometry = new THREE.BoxGeometry( 1, 1, 1 );
        let material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        this.cube = new THREE.Mesh( geometry, material );
        this.scene.add( this.cube );

        this.camera.position.z = 5;

        this.onUpdatade();

    }

    componentDidUpdate(prevProps, prevState, snapshot){
        if(prevProps.color !== this.props.config.color){
            //console.log(this.props.config.color);
            this.cube.material.color.setHex(this.props.config.color);
        }
        this.cube.scale.x = this.props.config.width;
        this.cube.scale.y = this.props.config.height;
    }

    onUpdatade(){
        requestAnimationFrame( () => {
            this.onUpdatade()
        });
        this.cube.rotation.x += .01;
        this.cube.rotation.y += .01;
        this.renderer.render( this.scene, this.camera );
    }

    render() {
        return (
            <canvas ref="canvas" className='CanvasRenderer'></canvas>
        );
    }
}

export default CanvasRenderer;
