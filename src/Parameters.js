import React, { Component } from 'react';
import './Parameters.scss';
import Slider from './Slider';
import SelectBox from './SelectBox'

class Parameters extends Component {
    constructor(){
        super();
        this.state = {
            width: 0
        }
    }
    onWidthChange(e){
        this.setState({
            width: e
        });
        this.props.onConfigChange({
            width: e
        })
    }

    onHeightChange(e){
        this.setState({
            height: e
        });
        this.props.onConfigChange({
            height: e
        });
    }

    onColorChange(e){
        console.log('test');
        this.setState({
            color: e
        });
        this.props.onConfigChange({
            color: e
        })
    }

    render() {
        return (
            <div className="Parameters">
                <Slider onChange={e => this.onWidthChange(e)} label='width'></Slider>
                <Slider onChange={e => this.onHeightChange(e)} label='height'></Slider>
                <SelectBox onChange={e => this.onColorChange(e)}></SelectBox>
            </div>
        );
    }
}

export default Parameters;
