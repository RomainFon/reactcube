import React, { Component } from 'react';
import './App.css';
import Parameters from './Parameters';
import CanvasRenderer from './CanvasRenderer';


class App extends Component {
  constructor(){
    super();
    this.state = {
      config: this.getConfig()
    }
  }

  getConfig(){
    if(window.localStorage.getItem('config')){
      return JSON.parse(window.localStorage.getItem('config'));
    } else{
        return{
            width: 1,
            height: 1,
            color: 0x00ff00
        }
    }
  }

  onConfigChange(newConfig){
    console.log( this.state.config)
    this.setState({
        config: {
            width: newConfig.width || this.state.config.width,
            height: newConfig.height || this.state.config.height,
            color: newConfig.color || this.state.config.color,
        }
    }, () => {
      window.localStorage.setItem('config', JSON.stringify(this.state.config));
      });
  }
  render() {
    return (
      <div className="App">
          <Parameters onConfigChange={config => this.onConfigChange(config)}></Parameters>
          <CanvasRenderer config={this.state.config}></CanvasRenderer>
      </div>
    );
  }
}

export default App;
