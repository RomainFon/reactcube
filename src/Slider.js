import React, { Component } from 'react';
import './Slider.scss';

class Slider extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: props.value || 2
        }
    }

    onChange(e){
        this.setState({
            value: e.currentTarget.value
        });
        if(this.props.onChange){
            this.props.onChange(Number(this.state.value));
        }
    }

    render() {
        return (
            <div className="Slider">
                <label>{ this.props.label }</label>
                <input type="range" min=".2" max="3" step=".1"
                       onChange={(e)=> this.onChange(e)}/>
            </div>
        );
    }
}

export default Slider;
